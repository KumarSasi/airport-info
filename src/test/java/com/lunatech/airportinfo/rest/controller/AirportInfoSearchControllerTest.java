/**
 * 
 */
package com.lunatech.airportinfo.rest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lunatech.airportinfo.service.AirportService;

/**
 * @author Sasikumar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AirportInfoSearchControllerTest {

	@InjectMocks
	AirportInfoSearchController airportInfoSearchController;

	@Mock
	private AirportService airportService;

	private MockMvc mockMvc;

	/**
	 * Test method for
	 * {@link com.lunatech.airportinfo.rest.controller.AirportInfoSearchController#getAirports(java.lang.String)}
	 * .
	 */
	@Test
	public void testGetAirportsByCountryCode() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(airportInfoSearchController).build();
		mockMvc.perform(get("/search/country/searchByCode/NL").contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetAirportsByCountryName() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(airportInfoSearchController).build();
		mockMvc.perform(get("/search/country/searchByName/Netherlands").contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}

}
