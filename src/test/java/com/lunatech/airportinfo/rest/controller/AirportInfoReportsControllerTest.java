/*
 * Copyright (c) KLM Royal Dutch Airlines. All Rights Reserved.
 * ============================================================
 */
package com.lunatech.airportinfo.rest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lunatech.airportinfo.service.AirportService;

/**
 * @author Sasikumar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AirportInfoReportsControllerTest {

    @InjectMocks
    AirportInfoReportsController airportInfoReportsController;

    @Mock
    private AirportService airportService;

    private MockMvc mockMvc;

    @Test
    public void testGenerateReport() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(airportInfoReportsController).build();
        mockMvc.perform(get("/reports/airports/maximum").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
    }

    @Test
    public void testGetTop10MostCommonRunwayLatitudes() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(airportInfoReportsController).build();
        mockMvc.perform(get("/reports/mostCommonRunways").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
    }

}
