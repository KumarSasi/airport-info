/**
 * 
 */
package com.lunatech.airportinfo.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.lunatech.airportinfo.AirportInfoApplicationTests;
import com.lunatech.airportinfo.domain.AirportCount;
import com.lunatech.airportinfo.domain.AirportServiceException;
import com.lunatech.airportinfo.domain.view.AirportDetailsVO;

/**
 * @author Sasikumar
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AirportInfoApplicationTests.class)
@EnableMongoRepositories
@WebAppConfiguration
public class AirportServiceImplTest {

	@Autowired
	private AirportService airportService;
	
	/**
	 * Test method for {@link AirportService#getAirportsByCountryName(String)}
	 * 
	 * This will test the function which collects all the airports for the given country name
	 * @throws AirportServiceException if more than one result found
	 */
	@Test
	public void testGetAirportsForTheGivenCountryName() throws AirportServiceException {
		List<AirportDetailsVO> airportDetailsVOs = airportService.getAirportsByCountryName("Netherlands");
		Assert.assertFalse(airportDetailsVOs.isEmpty());
	}
	
	/**
	 * Test method for {@link AirportService#getAirportsForByCountryCode(String)}
	 * 
	 * This will test the function which collects all the airports for the given country code
	 * 
	 * @throws AirportServiceException if more than one result found
	 */
	@Test
	public void testGetAirportsForTheGivenCountryCode() throws AirportServiceException {
		List<AirportDetailsVO> airportDetailsVOs = airportService.getAirportsForByCountryCode("NL");
		Assert.assertFalse(airportDetailsVOs.isEmpty());
	}
	
	
	/**
	 * Test method for {@link AirportService#generateHighestAndLowestAirportCountReport()}
	 * 
	 * This will test the function which collects all the airports for the given country code
	 * 
	 * @throws AirportServiceException if more than one result found
	 */
	@Test
	public void testGenerateHighestAndLowestAirportCountReport_Descending() throws AirportServiceException {
		List<AirportCount> airportCounts = airportService.generateHighestAndLowestAirportCountReport(true);
		//Assert.assertEquals(21501, airportCounts.get(0).getNumberOfAirports());
		Assert.assertFalse(airportCounts.isEmpty());
	}
	

}
