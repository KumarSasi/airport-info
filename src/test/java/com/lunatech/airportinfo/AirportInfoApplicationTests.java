package com.lunatech.airportinfo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.mongodb.MongoClient;

@ComponentScan(basePackages={"com.lunatech.airportinfo"})
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
@Configuration
public class AirportInfoApplicationTests {

	public @Bean
	MongoDbFactory mongoDbFactory() throws Exception {
		return new SimpleMongoDbFactory(new MongoClient(), "lunatech");
	}
}
