/**
 * 
 */
package com.lunatech.airportinfo.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lunatech.airportinfo.domain.AirportCount;
import com.lunatech.airportinfo.domain.CommonRunwayLatitudeCount;
import com.lunatech.airportinfo.service.AirportService;

/**
 * @author Sasikumar
 *
 */
@RestController
@RequestMapping(value="/reports")
public class AirportInfoReportsController {
	
	@Autowired	
	private AirportService airportService;

	/**
	 * 
	 * @param maxOrMin
	 * @return
	 */
	@RequestMapping(value = "/airports/{maxOrMin}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AirportCount> generateReport(@PathVariable String maxOrMin) {
		boolean descending = "maximum".equalsIgnoreCase(maxOrMin);
		List<AirportCount> report = airportService.generateHighestAndLowestAirportCountReport(descending);
		return report;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/mostCommonRunways", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CommonRunwayLatitudeCount> getTop10MostCommonRunwayLatitudes() {
		System.out.println("AirportInfoReportsController.getTop10MostCommonRunwayLatitudes()");
		List<CommonRunwayLatitudeCount> mostCommonRunways = airportService.getTop10MostCommonRunwayLatitudes();
		return mostCommonRunways;
	}
}
