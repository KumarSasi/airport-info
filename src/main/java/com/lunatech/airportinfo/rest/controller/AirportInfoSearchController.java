/**
 * 
 */
package com.lunatech.airportinfo.rest.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lunatech.airportinfo.AirportInfoResourceURI;
import com.lunatech.airportinfo.domain.AirportServiceException;
import com.lunatech.airportinfo.domain.JTableDataWrapper;
import com.lunatech.airportinfo.domain.view.AirportDetailsVO;
import com.lunatech.airportinfo.domain.view.RunwayVO;
import com.lunatech.airportinfo.service.AirportService;

/**
 * @author Sasikumar
 *
 */
@RestController
@RequestMapping(value = AirportInfoResourceURI.SEARCH_CONTROLLER_PATH)
public class AirportInfoSearchController {
	
	private static final Logger LOG = LoggerFactory.getLogger(AirportInfoSearchController.class);
	private static final String OK = "OK";

	@Autowired	
	private AirportService airportService;
	
	/**
	 * This method will get the airports and its runways details for the country name provided
	 * by the user.
	 * 
	 * @param name
	 * @return 
	 * @throws AirportServiceException 
	 */
	@RequestMapping(value = AirportInfoResourceURI.SEARCH_COUNTRY_NAME_PATH, method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public JTableDataWrapper<AirportDetailsVO> getAirportsBasedOnCountryName(@PathVariable String countryName) throws AirportServiceException {
		//TODO - null check
		System.out.println("AirportInfoSearchController.getAirports()");
		List<AirportDetailsVO> airportDetailsVOs = airportService.getAirportsByCountryName(countryName);
		JTableDataWrapper<AirportDetailsVO> dataWrapper = new JTableDataWrapper<>();
		dataWrapper.setResult(OK);
		dataWrapper.setRecords(airportDetailsVOs);
		return dataWrapper;
	}
	
	/**
	 * This method will get the airports and its runways details for the country code provided
	 * by the user.
	 * 
	 * @param name
	 * @return 
	 * @throws AirportServiceException 
	 */
	@RequestMapping(value = AirportInfoResourceURI.SEARCH_COUNTRY_CODE_PATH, method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	public JTableDataWrapper<AirportDetailsVO> getAirportsBasedOnCountryCode(@PathVariable String countryCode) throws AirportServiceException {
		//TODO - null check
		System.out.println("AirportInfoSearchController.getAirports()");
		List<AirportDetailsVO> airportDetailsVOs = airportService.getAirportsForByCountryCode(countryCode);
		JTableDataWrapper<AirportDetailsVO> dataWrapper = new JTableDataWrapper<>();
		dataWrapper.setResult(OK);
		dataWrapper.setRecords(airportDetailsVOs);
		return dataWrapper;
	}
	
	/**
	 * This method will collect all the runway details for the selected airport on screen
	 * 
	 * @param airportIdent
	 * @return the list of {@link RunwayVO}
	 * @throws AirportServiceException
	 */
	@RequestMapping(value = AirportInfoResourceURI.SEARCH_AIRPORT_PATH,  method = RequestMethod.GET)
	public JTableDataWrapper<RunwayVO> getRunwaysPerAirport(@PathVariable String airportIdent) throws AirportServiceException {
		//TODO - null check
		System.out.println("AirportInfoSearchController.getRunwaysPerAirport()");
		List<RunwayVO> runwayVOs = airportService.getRunwaysPerAirport(airportIdent);
		JTableDataWrapper<RunwayVO> dataWrapper = new JTableDataWrapper<>();
		dataWrapper.setResult(OK);
		dataWrapper.setRecords(runwayVOs);
		return dataWrapper;
	}
	
}
