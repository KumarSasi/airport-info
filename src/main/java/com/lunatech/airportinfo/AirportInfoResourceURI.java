/**
 * 
 */
package com.lunatech.airportinfo;

/**
 * @author Sasikumar
 *
 */
public final class AirportInfoResourceURI {

	public static final String HOME_PATH = "/";
	public static final String HOME_VIEW_NAME = "home";
	public static final String SEARCH_COUNTRY_NAME_PATH = "/country/searchByName/{countryName}";
	public static final String SEARCH_COUNTRY_CODE_PATH = "/country/searchByCode/{countryCode}";
	public static final String SEARCH_AIRPORT_PATH = "/airport/{airportIdent}";
	public static final String SEARCH_CONTROLLER_PATH = "/search";
	public static final String REPORTS_CONTROLLER_PATH = "/reports";
	public static final String REPORT_AIRPORTS = "/airports/{maxOrMin}";

	public static final String VIEW_PREFIX = "/WEB-INF/view/";
	public static final String VIEW_SUFFIX = ".html";

	public static final String RESOURCE_HANDLER_JS = "/js/**";
	public static final String RESOURCE_HANDLER_CSS = "/css/**";
	public static final String RESOURCE_HANDLER_IMAGE = "/images/**";
	
	public static final String RESOURCE_LOCATION_JS = "/js/";
	public static final String RESOURCE_LOCATION_CSS = "/css/";
	public static final String RESOURCE_LOCATION_IMAGE = "/images/";

}
