/**
 * 
 */
package com.lunatech.airportinfo.web.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lunatech.airportinfo.domain.AirportServiceException;
import com.lunatech.airportinfo.domain.view.ErrorVO;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sasikumar
 *
 */
@ControllerAdvice
@Slf4j
public class CommonControllerAdvice {

	@ExceptionHandler(AirportServiceException.class)
	@ResponseBody
	public ResponseEntity<ErrorVO> handleRunTimeException(AirportServiceException airportServiceException) {
		if(StringUtils.isEmpty(airportServiceException.getErrorCode())) {
			log.error("An exception occurred", airportServiceException);
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<ErrorVO>(new ErrorVO("An error has occurred"), responseHeaders, HttpStatus.MULTIPLE_CHOICES);
	}
	
}
