package com.lunatech.airportinfo.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airportinfo.domain.Airport;

@Repository
public interface AirportRepository  extends MongoRepository<Airport, Serializable>{

	public List<Airport> findByIsoCountry(String isoCountryCode);
}
