/**
 * 
 */
package com.lunatech.airportinfo.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airportinfo.domain.Country;

/**
 * @author Sasikumar
 *
 */
@Repository
public interface CountryRespository extends MongoRepository<Country, Serializable> {
	
	public List<Country> findByCodeIgnoreCase(String code);
	public List<Country> findByNameStartsWithIgnoreCase(String name);
	

}
