/**
 * 
 */
package com.lunatech.airportinfo.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.lunatech.airportinfo.domain.Runway;

/**
 * @author Sasikumar
 *
 */
@Repository
public interface RunwayRepository  extends MongoRepository<Runway, Serializable> {

	public List<Runway> findByAirportIdent(String ident);
}
