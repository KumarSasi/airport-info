/**
 * 
 */
package com.lunatech.airportinfo;

import java.io.IOException;
import java.net.URL;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.lunatech.airportinfo.domain.view.RunwayVO;
import com.lunatech.airportinfo.repository.CountryRespository;

/**
 * @author Sasikumar
 *
 */
public class LoadDatatoDB implements InitializingBean {
	
	@Autowired
	CountryRespository countryRepo;

	@Override
	public void afterPropertiesSet() throws Exception {
		URL resource = RunwayVO.class.getClassLoader().getResource("countries.json");

		try {
			Resources.toString(resource, Charsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
