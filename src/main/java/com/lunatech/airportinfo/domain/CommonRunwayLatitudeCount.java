/**
 * 
 */
package com.lunatech.airportinfo.domain;

import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

/**
 * @author Sasikumar
 *
 */
@Data
public class CommonRunwayLatitudeCount {
	
	@Field("_id")
	private String le_ident;
	private Long mostCommonRunways;

}
