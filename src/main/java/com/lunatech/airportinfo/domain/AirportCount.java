/**
 * 
 */
package com.lunatech.airportinfo.domain;

import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

/**
 * @author Sasikumar
 *
 */
@Data
public class AirportCount {
	
	@Field("_id")
	private String countryCode;
	private Long numberOfAirports;

}
