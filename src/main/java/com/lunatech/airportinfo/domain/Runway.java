/**
 * 
 */
package com.lunatech.airportinfo.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

/**
 * @author Sasikumar
 *
 */
@Document(collection="runways")
@Data
public class Runway {
	
	 private String id;
     private Long airport_ref;
     @Field("airport_ident")
     private String airportIdent;
     private Integer length_ft;
     private Integer width_ft;
     private String surface;
     private Integer lighted;
     private Integer closed;
     private String le_ident;
     private String le_latitude_deg;
     private String  le_longitude_deg;
     private String le_elevation_ft;
     private String le_heading_degT;
     private String le_displaced_threshold_ft;
     private String he_ident;
     private String he_latitude_deg;
     private String he_longitude_deg;
     private String he_elevation;
     private String he_heading_degT;
     private String he_displaced_threshold_ft;

}
