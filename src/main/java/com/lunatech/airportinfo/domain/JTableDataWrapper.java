/**
 * 
 */
package com.lunatech.airportinfo.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author 
 *
 */
public class JTableDataWrapper<T> {

    @JsonProperty("Result")
    private String result;
    @JsonProperty("Records")
    private List<T> records;
    @JsonProperty("TotalRecordCount")
    private long totalRecordCount; 
    
    
    public long getTotalRecordCount() {
        return totalRecordCount;
    }

    
    public void setTotalRecordCount(long totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    public String getResult() {
        return result;
    }
    
    public void setResult(String result) {
        this.result = result;
    }
    
    public List<T> getRecords() {
        return records;
    }
    
    public void setRecords(List<T> records) {
        this.records = records;
    }
    
}
