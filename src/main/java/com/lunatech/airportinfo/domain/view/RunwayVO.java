/**
 * 
 */
package com.lunatech.airportinfo.domain.view;

import lombok.Data;

/**
 * @author Sasikumar
 *
 */
@Data
public class RunwayVO {

	private Long airport_ref;
	private String airport_ident;
	private Integer length_ft;
	private Integer width_ft;
	private String surface;
	private String le_ident;
	
}
