/**
 * 
 */
package com.lunatech.airportinfo.domain.view;

/**
 * @author Sasikumar
 *
 */
public class ErrorVO {
    
    /** The error message. */
    private String errorMessage;
    

    public ErrorVO(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }



}
