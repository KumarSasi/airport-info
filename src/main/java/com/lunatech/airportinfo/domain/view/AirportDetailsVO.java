/**
 * 
 */
package com.lunatech.airportinfo.domain.view;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

/**
 * @author Sasikumar
 *
 */
@Data
@JsonInclude(Include.NON_EMPTY)
public class AirportDetailsVO {
	
	private String countryCode;
	private String countryName;
	private String continent;
	private BigInteger airportId;
	private String airportName;
	private String airportType;
	private String airportIdent;
	private String iata_code;
	private String home_link;
	private String wikipedia_link;
	private String municipality;
	private float latitude;
	private float longitude;
	
	private List<RunwayVO> runwayVO;

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AirportDetailsVO [countryCode=" + countryCode + ", continent=" + continent + ", airportName="
				+ airportName + ", iata_code=" + iata_code + ", home_link=" + home_link + ", wikipedia_link="
				+ wikipedia_link + ", municipality=" + municipality + "]";
	}
	
	

}
