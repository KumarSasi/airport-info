/**
 * 
 */
package com.lunatech.airportinfo.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * @author Sasikumar
 *
 */
@Document(collection="countries")
@Data
public class Country {
	
	@Id
	private String id;
	@Indexed
	private String code;
	@Indexed
	private String name;
	private String continent;
	private String wikipedia_link;
	private String keywords;

}
