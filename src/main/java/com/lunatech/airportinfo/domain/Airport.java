/**
 * 
 */
package com.lunatech.airportinfo.domain;

import java.math.BigInteger;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

/**
 * MongoDB document class
 * 
 * @author Sasikumar
 *
 */
@Document(collection="airports")
@Data
public class Airport {
	
	@Id
	@Field("id")
	private BigInteger id;
	@Indexed
	private String ident;
	private String type;
	private String name;
	private float latitude_deg;
	private float longitude_deg;
	private Integer elevation_ft;
	private String continent;
	@Indexed
	@Field("iso_country")
	private String isoCountry;
	private String iso_region;
	private String municipality;
	private String scheduled_service;
	private String gps_code;
	private String iata_code;
	private String local_code;
	private String home_link;
	private String wikipedia_link;
	private String keywrods;
	
}
