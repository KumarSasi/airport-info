/**
 * 
 */
package com.lunatech.airportinfo.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sasikumar
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class AirportServiceException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private String errorMessage;
	private String errorCode;
	private String marker;
	

	/**
	 * @param errorMessage
	 */
	public AirportServiceException(String marker, String errorMessage) {
		this(marker, errorMessage, null);

	}
	
	/**
	 * @param errorMessage
	 * @param errorCode
	 */
	public AirportServiceException(String marker, String errorMessage, String errorCode) {
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
		this.marker = marker;
	}
	
}
