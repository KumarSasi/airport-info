/**
 * 
 */
package com.lunatech.airportinfo.service;

import java.util.List;

import com.lunatech.airportinfo.domain.AirportCount;
import com.lunatech.airportinfo.domain.AirportServiceException;
import com.lunatech.airportinfo.domain.CommonRunwayLatitudeCount;
import com.lunatech.airportinfo.domain.view.AirportDetailsVO;
import com.lunatech.airportinfo.domain.view.RunwayVO;

/**
 * @author Sasikumar
 *
 */
public interface AirportService {
	
	public List<RunwayVO> getRunwaysPerAirport(String airportIdent);

	public List<AirportDetailsVO> getAirportsByCountryName(String name) throws AirportServiceException;
	
	public List<AirportDetailsVO> getAirportsForByCountryCode(String name) throws AirportServiceException;
	
	public List<AirportCount> generateHighestAndLowestAirportCountReport(boolean descending);
	
	public List<CommonRunwayLatitudeCount> getTop10MostCommonRunwayLatitudes();
	

}
