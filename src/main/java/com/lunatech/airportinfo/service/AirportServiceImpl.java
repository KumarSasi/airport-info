/**
 * 
 */
package com.lunatech.airportinfo.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.lunatech.airportinfo.domain.Airport;
import com.lunatech.airportinfo.domain.AirportCount;
import com.lunatech.airportinfo.domain.AirportServiceException;
import com.lunatech.airportinfo.domain.CommonRunwayLatitudeCount;
import com.lunatech.airportinfo.domain.Country;
import com.lunatech.airportinfo.domain.Runway;
import com.lunatech.airportinfo.domain.view.AirportDetailsVO;
import com.lunatech.airportinfo.domain.view.RunwayVO;
import com.lunatech.airportinfo.helper.AirportDetailsHelper;
import com.lunatech.airportinfo.repository.AirportRepository;
import com.lunatech.airportinfo.repository.CountryRespository;
import com.lunatech.airportinfo.repository.RunwayRepository;

/**
 * @author Sasikumar
 *
 */
@Service
public class AirportServiceImpl implements AirportService {

	@Autowired	
	private CountryRespository countryRespository;
	
	@Autowired
	private AirportRepository airportRespository;
	
	@Autowired
	private RunwayRepository runwayRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	/**
	 * This method will collect all the airports for the given country name
	 * @param name The country name input from the user
	 * @return List of {@link AirportDetailsVO}
	 */
	@Override
	public List<AirportDetailsVO> getAirportsByCountryName(String name) throws AirportServiceException {
		List<Country> countries = countryRespository.findByNameStartsWithIgnoreCase(name);
		List<AirportDetailsVO> airportDetailsVOs = populateAirportDetailsForCountry(countries); 
		return airportDetailsVOs;
	}
	
	/**
	 * This method will collect all the airports for the given country code
	 * @param code The country name input from the user
	 * @return List of {@link AirportDetailsVO}
	 */
	@Override
	public List<AirportDetailsVO> getAirportsForByCountryCode(String code) throws AirportServiceException {
		List<Country> countries = countryRespository.findByCodeIgnoreCase(code);
		List<AirportDetailsVO> airportDetailsVOs = populateAirportDetailsForCountry(countries); 
		return airportDetailsVOs;
	}

	/**
	 * @param countries
	 * @return
	 * @throws AirportServiceException
	 */
	private List<AirportDetailsVO> populateAirportDetailsForCountry(List<Country> countries)
			throws AirportServiceException {
		List<Airport> airports = null;
		List<AirportDetailsVO> airportDetailsVOs = new ArrayList<>();
		if (!CollectionUtils.isEmpty(countries)) {
			if (countries.size() > 1) {
				throw new AirportServiceException("BUSINESS", "More than one country found");
			}
			Country country = countries.get(0);
			if (!StringUtils.isEmpty(country.getCode())) {
				airports = airportRespository.findByIsoCountry(country.getCode());
			}
			transformDataToDTOAirports(airports, airportDetailsVOs, country.getName());
		}
		return airportDetailsVOs;
	}

	/**
	 * 
	 * @param airports
	 * @param airportDetailsVOs
	 */
	private List<AirportDetailsVO> transformDataToDTOAirports(List<Airport> airports, List<AirportDetailsVO> airportDetailsVOs, String countryName) {
		for (Airport airport: airports) {
			AirportDetailsVO airportDetailsVO = AirportDetailsHelper.transformAirportDomainToDTO(airport, countryName);
			airportDetailsVOs.add(airportDetailsVO);
		}
		return airportDetailsVOs;
	}

	/**
	 * This method retrieves all the runway information available for the given airport ident code
	 * 
	 * @param airportIdent the airport ident code
	 * @return List<RunwayVO> The list of runway info
	 */
	@Override
	public List<RunwayVO> getRunwaysPerAirport(String airportIdent) {
		List<Runway> runways = runwayRepository.findByAirportIdent(airportIdent);
		return AirportDetailsHelper.transformRunwayDomainToDTO(runways);
	}

	/**
	 * This method retruns a list of top 10 countries with maximum and minimum number of airports
	 * @param descending
	 * @return List<AirportCount> 
	 * 
	 */
	public List<AirportCount> generateHighestAndLowestAirportCountReport(boolean descending) {
		Aggregation agg = newAggregation(group("iso_country").count().as("numberOfAirports"),
				sort((descending ? Sort.Direction.DESC : Sort.Direction.ASC), "numberOfAirports"), limit(10));

		AggregationResults<AirportCount> results = mongoTemplate.aggregate(agg, "airports", AirportCount.class);
		List<AirportCount> airportCounts = results.getMappedResults();
		return airportCounts;
	}

	/**
	 * 
	 * This method will query the runways collection and find out the top 10 most common <br>
	 * runway latitudes (indicated as "le_ident" in runways collection) 
	 * 
	 *  @return List of most common runways
	 */
	@Override
	public List<CommonRunwayLatitudeCount> getTop10MostCommonRunwayLatitudes() {
		Aggregation agg = newAggregation(group("le_ident").count().as("mostCommonRunways"),
				sort(Sort.Direction.DESC, "mostCommonRunways"), limit(10));

		AggregationResults<CommonRunwayLatitudeCount> results = mongoTemplate.aggregate(agg, "runways", CommonRunwayLatitudeCount.class);
		List<CommonRunwayLatitudeCount> mostCommonRunways = results.getMappedResults();
		return mostCommonRunways;
	}
	
	
	
}
