package com.lunatech.airportinfo.helper;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.lunatech.airportinfo.domain.Airport;
import com.lunatech.airportinfo.domain.Runway;
import com.lunatech.airportinfo.domain.view.AirportDetailsVO;
import com.lunatech.airportinfo.domain.view.RunwayVO;

/**
 * @author Sasikumar
 *
 */
public final class AirportDetailsHelper {
	
	/**
	 * Domain to DTO transformation method
	 * 
	 * @param airports
	 * @return List<AirportDetailsVO>
	 */
	public static AirportDetailsVO transformAirportDomainToDTO(Airport airport, String countryName) {
		AirportDetailsVO airportDetailsVO = new AirportDetailsVO();
		airportDetailsVO.setAirportId(airport.getId());
		airportDetailsVO.setAirportName(airport.getName());
		airportDetailsVO.setAirportIdent(airport.getIdent());
		airportDetailsVO.setAirportType(airport.getType());
		airportDetailsVO.setContinent(airport.getContinent());
		airportDetailsVO.setCountryName(countryName);
		airportDetailsVO.setMunicipality(airport.getMunicipality());
		airportDetailsVO.setIata_code(airport.getIata_code());
		airportDetailsVO.setHome_link(airport.getHome_link());
		airportDetailsVO.setWikipedia_link(airport.getWikipedia_link());
		airportDetailsVO.setLatitude(airport.getLatitude_deg());
		airportDetailsVO.setLongitude(airport.getLongitude_deg());
		return airportDetailsVO;
	}
	
	public static List<RunwayVO> transformRunwayDomainToDTO(List<Runway> runways) {
		List<RunwayVO> runwayVOs = new ArrayList<>();
		for (Runway runway : runways) {
			RunwayVO runwayVO = new RunwayVO();
			runwayVO.setAirport_ident(runway.getAirportIdent());
			runwayVO.setAirport_ref(runway.getAirport_ref());
			runwayVO.setLength_ft(runway.getLength_ft());
			runwayVO.setWidth_ft(runway.getWidth_ft());
			runwayVO.setSurface(runway.getSurface());
			runwayVOs.add(runwayVO);
		}
		return runwayVOs;
	}

}
