/**
 * 
 */
$(document).ready(function() {
	defineHighestNumberofAirportTable();
	defineLowestNumberofAirportTable();
	defineMostCommonRunwayTable()
});
	function defineHighestNumberofAirportTable() {
		$.ajax({
		    url: '/reports/airports/maximum',
		    type: "GET",
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) {
		        // since we are using jQuery, you don't need to parse response
		        drawHighestNumberofAirportTable(data);
		    }
		});
	}

	function drawHighestNumberofAirportTable(data) {
	    for (var i = 0; i < data.length; i++) {
	    	drawHighestNumberofAirportTableRow(data[i]);
	    }
	}

	function drawHighestNumberofAirportTableRow(rowData) {
	    var row = $("<tr />")
	    $("#highestAirportCountTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
	    row.append($("<td>" + rowData.countryCode + "</td>"));
	    row.append($("<td>" + rowData.numberOfAirports + "</td>"));
	}
	
	function defineLowestNumberofAirportTable() {
		$.ajax({
		    url: '/reports/airports/minimum',
		    type: "GET",
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) {
		        // since we are using jQuery, you don't need to parse response
		        drawLowestNumberofAirportTable(data);
		    }
		});
	}
	
	function drawLowestNumberofAirportTable(data) {
	    for (var i = 0; i < data.length; i++) {
	    	drawLowestNumberofAirportTableRow(data[i]);
	    }
	}

	function drawLowestNumberofAirportTableRow(rowData) {
	    var row = $("<tr />")
	    $("#LowestAirportCountTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
	    row.append($("<td>" + rowData.countryCode + "</td>"));
	    row.append($("<td>" + rowData.numberOfAirports + "</td>"));
	}
	
	function defineMostCommonRunwayTable() {
		$.ajax({
		    url: '/reports/mostCommonRunways',
		    type: "GET",
		    dataType: "json",
		    success: function(data, textStatus, jqXHR) {
		        // since we are using jQuery, you don't need to parse response
		        drawMostCommonRunwayTable(data);
		    }
		});
	}
	
	function drawMostCommonRunwayTable(data) {
	    for (var i = 0; i < data.length; i++) {
	    	drawMostCommonRunwayTableRow(data[i]);
	    }
	}
	
	function drawMostCommonRunwayTableRow(rowData) {
	    var row = $("<tr />")
	    $("#mostCommonRunwayContainer").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
	    row.append($("<td>" + rowData.le_ident + "</td>"));
	    row.append($("<td>" + rowData.mostCommonRunways + "</td>"));
	}
	
	
	
