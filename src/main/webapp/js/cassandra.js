function defineProviderTransactionJtable() {
	 $("#resultContainer").jtable({
		title: ' ',
	    paging : true,
        pageSize : 23,
        selecting: true,
        multiselect :true,
        selectingCheckboxes : true,
        selectOnRowClick : false,
        saveUserPreferences : false,
        columnResizable : false,
        columnSelectable : false,
         actions: {
              listAction : function (postData, jtPrams) {
             	 return $.Deferred(function ($dfd) {
             	        $.ajax({
             	            url: 'cassandra/provider/transactions?searchKeys=' + postData + '&jtPageSize=' + jtPrams.jtPageSize + '&jtStartIndex=' + jtPrams.jtStartIndex + '&fromDateRange=' + $("#from").val() + '&toDateRange=' + $("#to").val(),
             	            type: 'GET',
             	            dataType: 'json',
             	            success: function (data) {
             	                $dfd.resolve(data);
             	            },
             	            error: function () {
             	                $dfd.reject();
             	            }
             	        });
             	    });
              }	
         },
         toolbar : {
         	 hoverAnimation: true,
             hoverAnimationDuration: 60,
             hoverAnimationEasing: undefined,
             items: [{
             	icon: 'images/download.png',
             	text: 'Download',
             	click : function () {
             		 var $selectedRows = $('#resultContainer').jtable('selectedRows');
             		   if ($selectedRows.length > 0) {
             			   	var sessionIds = [];
             			    $selectedRows.each(function () {
                                var providerTransaction = $(this).data('record');
                                sessionIds.push(providerTransaction.sessionId + '#' + providerTransaction.start.substring(0,10));
             			    });
             			    $("#sessionIdsToDownload").val(sessionIds);
             			    $("#downloadForm").submit();
             			   $("#sessionIdsToDownload").val('');
             		   }
            		return false;
             	  }
                }
             ]
         },
         fields: {
         	sessionId: {
                 key: true,
                 list: false
             },
             ExcludeItems :{
            		title : '',
                	width : '2%',
                 	display : function (providerTransaction) {
                 		var $img = $('<img src="images/expand.png" title="Transactions" style="cursor : pointer;"/>');
                 		$img.click(function() {
               			 $('#resultContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'External Transactions',
                                        actions: {
                                            listAction : function (postData, jtPrams) {
                                            	 return $.Deferred(function ($dfd) {
                                            	        $.ajax({
                                            	            url: 'cassandra/provider/transactions/'+ providerTransaction.record.id +'/consumer/transactions',
                                            	            type: 'GET',
                                            	            dataType: 'json',
                                            	            success: function (data) {
                                            	                $dfd.resolve(data);
                                            	            },
                                            	            error: function () {
                                            	                $dfd.reject();
                                            	            }
                                            	        });
                                            	    });
                                             }	
                                        },toolbar : {
                                        	hoverAnimation: true,
                                           hoverAnimationDuration: 60,
                                           hoverAnimationEasing: undefined,
                                           items: [{
                                        	 	icon: 'images/download.png',
                                               	text: 'Download',
                                               	click : function () {
                                               		window.location.href = 'cassandra/download/'+ providerTransaction.record.sessionId;
                                            		return false;
                                             	    }
                                                }
                                           ]
                                       },
                                        fields: {
                                          start : {
                                       		 key : true,
                                       		 list : true
                                       	    },
                                       	  serviceName: {
                                                title: 'Service',
                                                width: '25%',
                                                display : function(data) {
                                               	 if (data.record.responseTransformation === true) {
                                               		 return '<span class="requestResponseView" onClick="javascript:showRequestResponse(\''+ data.record.start + '\',\'' + data.record.serviceName + '\',\'' + data.record.requestId + '\',\'' + data.record.responseId + '\')">' + data.record.serviceName + '</span>' 
                                               		+ ' <span class="transformLink" onclick="javascript:showMessage(\''+ data.record.start + '\',\'' + data.record.serviceName + '\',\'' + data.record.responseId + '\')">View*</span>';
                                               	 } else {
                                               		 return '<span class="requestResponseView" onClick="javascript:showRequestResponse(\''+ data.record.start + '\',\'' + data.record.serviceName + '\',\'' + data.record.requestId + '\',\'' + data.record.responseId + '\')">' + data.record.serviceName + '</span>';
                                               	 }
                                                }
                                            },
                                            latency: {
                                           	 title : 'Latency',
                                           	 width : '30%'
                                            }
                                           }
                                    }, function (data) {
                                        data.childTable.jtable('load');
                                    });
                        
                 		});
                 		return $img;
                 	}
             },
             start: {
                 title: 'Timestamp',
                 width: '12%'
             },
             host: {
                 title: 'Host',
                 width: '5%'
             },
             channel: {
                 title: 'Channel',
                 width: '5%'
             },
             eventName: {
                 title: 'Event',
                 width: '28%'
             },
             serviceName: {
                 title: 'Service',
                 width: '20%',
                 display : function(data) {
                	 if (data.record.responseTransformation === true) {
                		 return '<span class="requestResponseView" onClick="javascript:showRequestResponse(\''+ data.record.start + '\',\''+ data.record.serviceName + '\',\'' + data.record.id + '\',\'' + data.record.responseId + '\')">' + data.record.serviceName + '</span>'
                		 + ' <span class="transformLink" onclick="javascript:showMessage(\''+ data.record.start + '\',\''+  data.record.serviceName + '\',\'' + data.record.responseId + '\')">View*</span>';
                	 } else {
                		 return '<span class="requestResponseView" onClick="javascript:showRequestResponse(\''+ data.record.start + '\',\''+ data.record.serviceName + '\',\'' + data.record.id + '\',\'' + data.record.responseId + '\')">' + data.record.serviceName + '</span>';
                	 }
                 }
             },
             latency : {
            	 title: 'Latency',
                 width: '7%',
                 display : function (data) {
                	 return '<span style="float: right;">' + data.record.latency + '</span>';
                 }
             }
             , errorCodes: {
                 title: 'Error (if any)',
                 width: '21%',
                 display : function (data) {
                	 if (null != data.record.errorCodes) {
                		 return '<span style="color: red;">' + data.record.errorCodes + '</span>';
                	 } else {
                		 return '';
                	 }
                 }
             }
         }
	   });
}


function showMessage(startDateTime, serviceName, responseId) {
	$( "#resultPopup" ).dialog({
		height : 800,
		width : 1550,
		modal: true,
		resizable: true
		});
	xml = loadXMLDoc("cassandra/transactions/transformation/response/" + responseId + "/datetime/" + startDateTime);
	xsl = loadXMLDoc("cassandra/transactions/transformation/xsl/service/" + serviceName);
	// code for IE
	if (window.ActiveXObject || xhttp.responseType == "msxml-document") {
		ex = xml.transformNode(xsl);
		document.getElementById("transformationContent").innerHTML = ex;
	}
	// code for Chrome, Firefox, Opera, etc.
	else if (document.implementation && document.implementation.createDocument) {
		xsltProcessor = new XSLTProcessor();
		xsltProcessor.importStylesheet(xsl);
		resultDocument = xsltProcessor.transformToFragment(xml, document);
		$("#transformationContent").html(resultDocument);
	}
}

function showRequestResponse(startDateTime, serviceName, requestId, responseId) {
	$( "#requestResponseXML" ).dialog({
		height : 800,
		width : 1450,
		modal: true,
		resizable: true
		});
	$.getJSON( "cassandra/transactions/xml/request/" + requestId + "/response/" + responseId + "/xml/datetime/" + startDateTime, function(data) {
		$('#requestResponseXMLAccordion').accordion().show();
		$('#xmlRequestHeader').html(serviceName + '<span> : Request</span>');
		$('#requestXMLPreTag').text(vkbeautify.xml(data.requestXMLPayload));
		$('#xmlResponseHeader').html(serviceName + '<span> : Response</span>');
		if (null != data.responseXMLPayload) {
			$('#responseXMLPreTag').text(vkbeautify.xml(data.responseXMLPayload));
		} else {
			$('#responseXMLPreTag').text('--NA--');
		}
	});
	return false;
}

function loadXMLDoc(filename) {
	if (window.ActiveXObject) {
		xhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} else {
		xhttp = new XMLHttpRequest();
	}
	xhttp.open("GET", filename, false);
	try {
		xhttp.responseType = "msxml-document"
	} catch (err) {
	} // Helping IE11
	xhttp.send("");
	return xhttp.responseXML;
}