/**
 * 
 */
$(document).ready(function() {
	var countryInput;
	var countryDropDownSelectedValue;
	var searchMode;
	
	$('#AirportTableContainer').jtable({
		title : 'List of Airports',
		columnResizable : false,
		columnSelectable : false,
		actions : {
			listAction : function() {
				return $.Deferred(function($dfd) {
					$.ajax({
						url : '/search/country/'+ searchMode + countryInput,
						type : 'GET',
						dataType : 'json',
						success : function(data) {
							$dfd.resolve(data);
						},
						error : function() {
							$dfd.reject();
						}
					});
				});
			}},
			fields : {
				airportIdent : {
					key : true,
					list : false
				},
				runways : {
					title : 'Runways',
					widht : '2%',
					display : function(airportDetailsVO) {

                 		var $img = $('<img src="images/flight-icon.png" title="Runways" style="cursor : pointer;"/>');
                 		$img.click(function() {
               			 $('#AirportTableContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'List of Runways',
                                        actions: {
                                            listAction : function () {
                                            	 return $.Deferred(function ($dfd) {
                                            	        $.ajax({
                                            	            url: '/search/airport/'+  airportDetailsVO.record.airportIdent,
                                            	            type: 'GET',
                                            	            dataType: 'json',
                                            	            success: function (data) {
                                            	                $dfd.resolve(data);
                                            	            },
                                            	            error: function () {
                                            	                $dfd.reject();
                                            	            }
                                            	        });
                                            	    });
                                             }	
                                        },
                                        fields: {
                                        	airport_ident : {
                                       		 key : true,
                                       		 list : true
                                       	    },
                                       	 length_ft : {
                                       	    	title : 'length_ft',
                                              	width : '30%'
                                       	    },
                                       	 width_ft : {
                                       	    	title : 'width_ft',
                                              	width : '30%'
                                       	    },
                                         surface: {
                                           	 	title : 'Surface',
                                           	 	width : '30%'
                                            }
                                           }
                                    }, function (data) {
                                        data.childTable.jtable('load');
                                    });
                        
                 		});
                 		return $img;
					}
				},
				airportName : {
					title : 'Airport Name',
					width : '10%'
				},
				airportType : {
					title : 'Airport Type',
					width : '10%'
				},
				latitude : {
					title : 'Latitude',
					width : '10%'
				},
				longitude : {
					title : 'Longitude',
					width : '10%'
				},
				continent : {
					title : 'Continent',
					width : '10%'
				},
				municipality : {
					title : 'Municipality',
					width : '10%',
				},
				home_link : {
					title : 'Home Link',
					width : '15%',
				},
				wikipedia_link : {
					title : 'Wikipedia Link',
					width : '15%',
				}
			}
	});

	$("#searchButton").click(function() {
		if ($('#countryCodeOrName').val() == '') {
			$("#message").text("Please enter the value").css({ 'color': 'red', 'font-size': '100%' });
		} else {
			$("#message").text('');
			countryDropDownSelectedValue = $('select[id=countryInputDropDown]').val();
			if (countryDropDownSelectedValue != null && countryDropDownSelectedValue != undefined && countryDropDownSelectedValue == 'code') {
				countryInput = $('#countryCodeOrName').val();
				searchMode = 'searchByCode/';
			} else {
				countryInput = $('#countryCodeOrName').val();
				searchMode = 'searchByName/';
			}
			$('#AirportTableContainer').jtable('load');
		}
	});

});
